jQuery(document).ready(function($){
	$('.slider-banner').slick({
		dots:true,
		arrows:false,
	});
	$('.projects-slider').slick({
		dots:true,
		arrows:false,
		speed: 500,
		fade: true,
		cssEase: 'linear'
	});
});
