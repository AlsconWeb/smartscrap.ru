<?php

/**

 * Шаблон для главной (page-custom.php)

 * @package WordPress

 * @subpackage your-clean-template-3

 * Template Name: Шаблон для главной

 */

$id = $post->ID;

$title_b2 = get_field('b_2_title', $id);
$button_text_b2 = get_field('b_2_text_button', $id);

$background_b3 = get_field('b_2_background', $id);
$title_b3 = get_field('b_3_title', $id);
$subtitle_b3 = get_field('b_3_subtitle', $id);
$iframe_b3 = get_field('b_3_iframe_video', $id);
$annotation_video_b3 = get_field('b_3_video_annotation', $id);
$btn_text_b3 = get_field('b_3_text_btn', $id);
$btn_link_b3 = get_field('b_3_link_btn', $id);


get_header(); // подключаем header.php ?>





<section class="b-cover">

    <ul class="b-cover__slider js-slider">

        <li style="background-image:url(http://smartscrap.net/wp-content/uploads/2017/07/smartscrap_slide_04.jpg)"></li>

        <li style="background-image:url(http://smartscrap.net/wp-content/uploads/2017/07/smartscrap_slide_05_01.jpg)"></li>

        <li style="background-image:url(http://smartscrap.net/wp-content/uploads/2017/07/smartscrap_slide_06_01.png)"></li>

    </ul>

    <div class="b-cover__inner">

        <div class="b-cover__side js-slider-dots">

            <!--<h1 class="b-cover__title"><?php the_field("title_slider"); ?></h1>

                    <p class="b-cover__description"><?php the_field("text-slider"); ?></p>										

					<p class="b-cover__description2"><?php the_field("text-slider2"); ?></p>-->

        </div>

        <div class="b-cover__overlay">



            <div class="b-cover__item b-cover__item1">



                <div class="b-cover__item-icon b-cover__item-icon1" style="">



                    <span class="large"></span>



                    <span class="small"></span>



                </div>



                <p class="b-cover__item-desc b-cover__item-desc1">Наш материал дешевле черного металлолома, а по многим техническим параметрам

                    превосходит его. <a href="#text1">Узнать больше…</a></p>



            </div>



            <div class="b-cover__item b-cover__item1">



                <div class="b-cover__item-icon b-cover__item-icon1" style="">



                    <span class="large"></span>



                    <span class="small"></span>



                </div>



                <p class="b-cover__item-desc b-cover__item-desc1">На 3% ниже себестоимость производства. Использование нашего продукта при

                    техническом сопровождении снижает себестоимость продукции на металлургическом

                    заводе. <a href="#text2">Узнать больше…</a></p>



            </div>



            <div class="b-cover__item b-cover__item1">



                <div class="b-cover__item-icon b-cover__item-icon1" style="">



                    <span class="large"></span>



                    <span class="small"></span>



                </div>



                <p class="b-cover__item-desc b-cover__item-desc1">№ 1 в оценке качества шлаков в мире. Никто на рынке не занимается оценкой и

                    контролем качества продукта лучше, чем мы. <a href="#text3">Узнать больше…</a></p>



            </div>



            <div class="b-cover__item b-cover__item1">



                <div class="b-cover__item-icon b-cover__item-icon1" style="">



                    <span class="large"></span>



                </div>



                <p class="b-cover__item-desc b-cover__item-desc1">На 20% снижен расход электроэнергии. Наши технические специалисты готовы оказать

                    техническое сопровождение при переплавке пробных партий и в процессе регулярной

                    работы. <a href="#text4">Узнать больше…</a></p>



            </div>



            <!-- <div class="b-cover__item b-cover__item1">



                        <div class="b-cover__item-icon b-cover__item-icon1">



                            Эксперты



                        </div>



                        <p class="b-cover__item-desc b-cover__item-desc1">в области качества<br>материала</p>



                    </div> !-->



        </div>

    </div>

</section>



<div class="b-promo">

    <div class="container">

        <div class="section-title">
            <h2><?php echo $title_b2; ?></h2>

        </div>

        <ul class="b-cover__controls" style="text-align: center;">
            <li><a class="btn btn-fill" href="#callback"><?php echo $button_text_b2;?></a></li>
        </ul>



    </div>

</div>

<div class="b-cover block-video" style="background: url(<?php echo $background_b3; ?>) no-repeat 50% 0; background-size: cover;">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg">
                <div class="block-left">
                    <h3><?php echo $title_b3;?></h3>
                    <p><?php echo $subtitle_b3;?></p>
                </div>
                <div class="block-right">
                    <div class="video"><?php echo $iframe_b3;?></div>
                    <div class="text-block">
                        <p><?php echo $annotation_video_b3;?></p>
                        <a href="<?php echo $btn_link_b3;?>" class="btn btn-yellow btn-fill"><?php echo $btn_text_b3;?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="b-reg">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <div class="row">

                    <div class="col-md-8">

                        <div class="b-reg_desc">

                            <h3>Зарегистрируйся и стань участником нашей партнёрской программы для трейдеров</h3>

                        </div>

                    </div>



                    <div class="col-md-4">

                        <div class="row b-reg_wrap">

                            <h4>Форма регистрации</h4>

                            <?php echo do_shortcode("[contact-form-7 id='247' title='Стать участником']"); ?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>





<?php get_footer();  ?>