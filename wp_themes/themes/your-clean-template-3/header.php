<?php

/**

 * Шаблон шапки (header.php)

 * @package WordPress

 * @subpackage your-clean-template-3

 */
?>

<!DOCTYPE html>

<html <?php language_attributes(); // вывод атрибутов языка ?>>

<head>

    <meta charset="<?php bloginfo( 'charset' ); // кодировка ?>">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="skype_toolbar" content="skype_toolbar_parser_compatible">



    <link rel="shortcut icon" href="/wp-content/themes/your-clean-template-3/img/favicon.ico">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&subset=cyrillic">



    <?php /* RSS и всякое */ ?>

    <link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">

    <link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">

    <link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">

    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />



    <?php /* Все скрипты и стили теперь подключаются в functions.php */ ?>



    <!--[if lt IE 9]>

	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>

	<![endif]-->



    <script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {

            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),

            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)

    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');



    ga('create', 'UA-98622472-1', 'auto');

    ga('send', 'pageview');
    </script>





    <?php wp_head(); // необходимо для работы плагинов и функционала ?>

</head>

<body <?php body_class(); // все классы для body ?>>



    <div class="page">

        <header class="header">

            <div class="container">

                <div class="header_top">

                    <div class="header_logo">

                        <a href="/">

                            <img src="<?php echo get_template_directory_uri(); ?>/img/logo260.png" alt="">

                        </a>

                    </div>

                    <?php dynamic_sidebar( 'top-area-slogan' ); ?>

                    <div class="header_info">

                        <a href="tel:+3 572 200-08-57" class="header_phone">
                            <div class="textwidget">+3 572 200-08-57</div>
                        </a><Br>
                        <a href="tel:+74993504109" class="header_phone">
                            <div class="textwidget">+7 499 350-41-09</div>
                        </a>


                    </div>
                    <div class="header_info">



                        <a href="#callback" class="btn btn-fill header_callback">Заказать обратный звонок</a>
                        <br>
                        <ul id="top-nav-ul" class="nav navbar-nav top-menu old1">
                            <li id="" class="soc-icon facebook menu-item menu-item-type-custom menu-item-object-custom menu-item-289"><a
                                    target="_blank" href="https://www.facebook.com/smartscrap1/">Мы в Facebook</a></li>
                            <li id="" class="soc-icon twitter menu-item menu-item-type-custom menu-item-object-custom menu-item-290"><a
                                    target="_blank" href="https://twitter.com/SmartScrapMan">Мы в Twitter</a></li>
                            <li id="" class="soc-icon linkedin menu-item menu-item-type-custom menu-item-object-custom menu-item-291"><a
                                    target="_blank" href="https://www.linkedin.com/in/andrey-korablin-smartscrap/">Мы в LinkedIn</a></li>
                            <li id="" class="soc-icon youtube menu-item menu-item-type-custom menu-item-object-custom menu-item-292"><a
                                    target="_blank" href="https://www.youtube.com/channel/UCL_lPGsYm_44clMwBUDH8ag">Мы в YouTube</a></li>
                        </ul>

                    </div>

                    <div class="header_lang">

                        <ul>

                            <li><a href="http://smartscrap.net/"><span class="flag-en"><img
                                            src="<?php echo get_template_directory_uri(); ?>/img/flags/en.png" alt=""></span></a></li>

                            <!--i><a href="http://smartscrap.pl/"><span class="flag-pl"><img src="<?php echo get_template_directory_uri(); ?>/img/flags/pl.png" alt=""></span></a></li>-->

                        </ul>

                    </div>

                </div>

            </div>

            <div class="header_nav">

                <div class="container">

                    <div class="collapse navbar-collapse" id="topnav">

                        <?php $args = array( // опции для вывода верхнего меню, чтобы они работали, меню должно быть создано в админке

								'theme_location' => 'top', // идентификатор меню, определен в register_nav_menus() в functions.php

								'container'=> false, // обертка списка, тут не нужна

						  		'menu_id' => 'top-nav-ul', // id для ul

						  		'items_wrap' => '<ul id="%1$s" class="nav navbar-nav %2$s">%3$s</ul>',

								'menu_class' => 'top-menu', // класс для ul, первые 2 обязательны

						  		'walker' => new bootstrap_menu(true) // верхнее меню выводится по разметке бутсрапа, см класс в functions.php, если по наведению субменю не раскрывать то передайте false		  		

					  			);

								wp_nav_menu($args); // выводим верхнее меню

							?>

                    </div>

                </div>

            </div>

            <div class="b-ticker">

                <div class="container">

                    <div class="b-ticker__wrap">

                        <div class="marquee"><span>Current index HMS 1/2 Turkey USD300</span></div>

                    </div>

                </div>

            </div>

        </header>