<?php

require_once './class.phpmailer.php';

$mail = new PHPMailer;
$mail->CharSet = 'utf-8';
$mail->setLanguage('ru', './phpmailer.lang-ru.php');
$mail->isHTML(true);

$response = array('error' => false, 'message' => '');

$form_data = new stdClass;
$form_data->title = isset($_POST['title']) ? $_POST['title'] : null;
$form_data->name  = isset($_POST['name'])  ? $_POST['name']  : null;
$form_data->email = isset($_POST['email']) ? $_POST['email'] : null;
$form_data->phone = isset($_POST['phone']) ? $_POST['phone'] : null;

if ($form_data->name) {

    $mail->setFrom('support@smartscrap.ru', 'SmartScrap');
    $mail->addReplyTo('support@smartscrap.ru', 'SmartScrap');

    $mail->addAddress('kan@smartscrap.ru');
    $mail->addAddress('korablina_e@bk.ru');
    $mail->addAddress('viazemsk@list.ru');

    $body = '<ul>';
    $body .= '<li><b>Имя: </b>' . $form_data->name . '</li>';
    if ($form_data->email) {
        $body .= '<li><b>Email: </b>' . $form_data->email . '</li>';
    }
    $body .= '<li><b>Телефон: </b>' . $form_data->phone . '</li>';
    $body .= '<ul>';

    $mail->Subject = $form_data->title;
    $mail->Body = $body;

    if (!$mail->send()) {
        $response['error'] = true;
        $response['message'] = 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        $response['message'] = 'Сообщение успешно отправлено!';
    }

    echo json_encode($response);
    return;
}

$response['error'] = true;
$response['message'] = 'Ошибка! Не все поля были заполнены!';
echo json_encode($response);
