<?php
/**
 * Страница - Мы покупаем (page-we-buy.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: Страница - Мы покупаем
 */




 
get_header();?>




<section class="section b-suppliers">

    <div class="b-suppliers_main">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-9">
                    <p class="b-suppliers_intro">
                        Мы заинтересованны<br>
                        в новых надежных поставщиках<br>
                        скрапа и других металлургических<br>
                        отходов.
                    </p>
                    <p class="b-suppliers_subintro">
                        Если у Вас есть металлургические отходы, я думаю, что мы сможем Вам быть полезны в их реализации.
                        Свяжитесь с нами и мы обсудим варианты сотрудничества.
                    </p>
                </div>
                <div class="col-md-4 col-lg-3">
                    <?php echo do_shortcode("[contact-form-7 id='232' title='Мы покупаем']"); ?>

                </div>
            </div>
        </div>
    </div>


</section>


<?php get_footer(); // подключаем footer.php ?>