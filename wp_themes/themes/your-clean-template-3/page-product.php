<?php
/**
 * Страница с шаблоном товаров (page-product.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: Страница с шаблоном товаров
 */

 $id = $post->ID;

 $kntrol_video = get_field('kntrol_video', $id);
 $kntrol_text = get_field('kntrol_text', $id);
 $kntrol_btn_text = get_field('kntrol_btn_text', $id);
 $kntrol_btn_link = get_field('kntrol_btn_link', $id);
 $kntrol_title = get_field('kntrol_title', $id);
 $kntrol_subtitle = get_field('kntrol_subtitle', $id);
get_header(); // подключаем header.php ?>


<div class="b-cover">
    <div id="slideshow" class="b-cover__slider slick-initialized slick-slider">
        <div aria-live="polite" class="slick-list draggable">
            <div class="slick-track" style="opacity: 1; width: 100%;" role="listbox">
                <div class="b-cover__slide slick-slide slick-current slick-active"
                    style="background-image: url(&quot;<?php the_field("background"); ?>&quot;); width: 100%; position: relative; left: 0px; top: 0px; z-index: 1000;"
                    data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide00"></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg">
                <div class="b-cover__lside">
                    <div>
                        <div class="b-cover__price"><span><?php the_field("price"); ?></span></div>
                    </div>
                    <div>
                        <div class="b-cover__subtitle"><span><?php the_field("price2"); ?></span></div>
                    </div>

                    <h1 class="b-cover__title"><?php the_title(); // заголовок поста ?></h1>
                    <p class="b-cover__subtitle"><?php the_field("under_title"); ?></p>
                    <ul class="b-cover__controls">
                        <li><a href="#callback" class="btn btn-fill">Заказать обратный звонок</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg">
                <div class="b-cover__rside">
                    <?php the_field("text_right"); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="section b-advantages">
    <div class="container title-line">
        <div class="title-line-left"></div>
        <div class="title-line-right"></div>
    </div>
    <div class="section-title">
        <h2>Описания товара -<?php the_title(); // заголовок поста ?></h2>
    </div>
    <div class="container">
        <div class="row">
            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
            <?php the_content(); // контент ?>
            <?php endwhile; // конец цикла ?>
        </div>
    </div>
</section>

<div id="b-gallery" class="b-gallery">
    <div class="container title-line">
        <div class="title-line-left"></div>
        <div class="title-line-right"></div>
    </div>
    <div class="section-title">
        <h2>Фотографии товара -<?php the_title(); // заголовок поста ?></h2>
    </div>
    <div class="container">
        <ul class="b-gallery__items">
            <li class="b-gallery__item">
                <a href="<?php the_field("img_product_1_full"); ?>" class="b-gallery__link" data-lightbox="gallery">
                    <img src="<?php the_field("img_product_1"); ?>" alt="Cast iron blocks">
                </a>
            </li>
            <li class="b-gallery__item">
                <a href="<?php the_field("img_product_2_full"); ?>" class="b-gallery__link" data-lightbox="gallery">
                    <img src="<?php the_field("img_product_2"); ?>" alt="Cast iron blocks">
                </a>
            </li>
            <li class="b-gallery__item">
                <a href="<?php the_field("img_product_3_full"); ?>" class="b-gallery__link" data-lightbox="gallery">
                    <img src="<?php the_field("img_product_3"); ?>" alt="Cast iron blocks">
                </a>
            </li>
        </ul>
    </div>
</div>

<section class="section b-attribs">
    <div class="container title-line">
        <div class="title-line-left"></div>
        <div class="title-line-right"></div>
    </div>
    <div class="section-title b-attribs__title">
        <h2>Технические характеристики</h2>
    </div>
    <div class="container">
        <p class="b-attribs__desc">
            <?php the_field("product_teh"); ?>
        </p>
    </div>
</section>

<section class="section b-properties">
    <div class="container title-line">
        <div class="title-line-left"></div>
        <div class="title-line-right"></div>
    </div>
    <div class="section-title b-properties__title">
        <h2>Химические свойства</h2>
    </div>
    <div class="container">
        <div class="b-properties__inner">
            <?php the_field("him_sostav"); ?>
        </div>
    </div>
</section>

<section class="section b-applications">
    <div class="container title-line">
        <div class="title-line-left"></div>
        <div class="title-line-right"></div>
    </div>
    <div class="section-title b-applications__title">
        <h2>Возможные применения:</h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="b-applications__item">
                    <img class="b-applications__item-image" src="<?php echo get_template_directory_uri(); ?>/img/b-applications/item111.png" alt="">
                    <p class="b-applications__item-desc">
                        <b>В автомобильных двигателях любой конфигурации.</b>
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="b-applications__item">
                    <img class="b-applications__item-image" src="<?php echo get_template_directory_uri(); ?>/img/b-applications/item222.png" alt="">
                    <p class="b-applications__item-desc">
                        <b>Сантехническое оборудование: радиаторы отопления, трубы, фитинги, раковины, кухонные мойки.</b>
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="b-applications__item">
                    <img class="b-applications__item-image" src="<?php echo get_template_directory_uri(); ?>/img/b-applications/item333.png" alt="">
                    <p class="b-applications__item-desc">
                        <b>Художественные шедевры.</b>
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="b-applications__item">
                    <img class="b-applications__item-image" src="<?php echo get_template_directory_uri(); ?>/img/b-applications/item444.png" alt="">
                    <p class="b-applications__item-desc">
                        <b>Оптимальная замена металлолома.</b>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="section b-applications">
    <div class="container title-line">
        <div class="title-line-left"></div>
        <div class="title-line-right"></div>
    </div>
    <div class="section-title b-applications__title">
        <h2>Наши преимущества:</h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="b-applications__item">
                    <img class="b-applications__item-image" src="<?php echo get_template_directory_uri(); ?>/img/b-applications/item55.png" alt="">
                    <p class="b-applications__item-desc">
                        <b>Вы зарабатываете до 200 000$ в месяц с гарантией. Наш продукт на 30% дешевле, чем традиционный лом.</b>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="b-applications__item">
                    <img class="b-applications__item-image" src="<?php echo get_template_directory_uri(); ?>/img/b-applications/item66.png" alt="">
                    <p class="b-applications__item-desc">
                        <b>Поставляем скрап в полном соответсвии с оговоренными условиями или возвращаем деньги.<br>
                            <a href="/kontrol-kachestva">Видео "Как мы контролируем качество каждой партии"</a></b>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="b-applications__item">
                    <img class="b-applications__item-image" src="<?php echo get_template_directory_uri(); ?>/img/b-applications/item77.png" alt="">
                    <p class="b-applications__item-desc">
                        <b>Мы предлагаем только проверенные источники материалов.</b>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="b-delivery" class="section b-delivery">
    <div class="container">
        <div class="title-line">
            <div class="title-line-left"></div>
            <div class="title-line-right"></div>
        </div>
        <div class="section-title b-delivery_title">
            <h2>Виды поставок</h2>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="b-delivery_item">
                    <div>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/b-delivery/item1.png" alt="">
                    </div>
                    <p>Железнодорожные<br>поставки</p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="b-delivery_item">
                    <div>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/b-delivery/item2.png" alt="">
                    </div>
                    <p>Автопоставки</p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="b-delivery_item">
                    <div>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/b-delivery/item3.png" alt="">
                    </div>
                    <p>Корабельные<br>отгрузки</p>
                </div>
            </div>
        </div>
        <div class="b-delivery_btn">
            <a href="#calc-cost" class="btn btn-big">Рассчитать стоимость товара с доставкой</a>
        </div>
    </div>
</section>
<section>
    <div class="b-video in-page" id="b-video">
        <div class="container">
            <div class="section-title">
                <h2><?php echo $kntrol_title; ?></h2>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="row">
                        <div class="b-video_wrap">
                            <?php echo $kntrol_video; ?>
                        </div>
                    </div>
                    <p style="    font-size: 24px;
    text-align: center;
    margin-top: -10px;"><?php echo $kntrol_text; ?></p>
                    <h3 style="    text-align: center;
    margin-top: 30px;
    font-size: 31px;color: #57707B;"><?php echo $kntrol_subtitle; ?></h3>
                    <div style="    text-align: center;
    margin-top: 20px;"><a href="<?php echo $kntrol_btn_link; ?>" target="_blank"
                            class="btn btn-fill"><?php echo $kntrol_btn_text; ?></a></div>
                </div>
            </div>
        </div>
    </div>

</section>


<?php get_footer(); ?>