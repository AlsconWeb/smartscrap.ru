<?php
/**
 * Страница - Мы продаем (page-we-sold.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: Страница - Мы продаем
 */

$id = $post->ID;

$kntrol_video = get_field('kntrol_video', $id);
$kntrol_text = get_field('kntrol_text', $id);
$kntrol_btn_text = get_field('kntrol_btn_text', $id);
$kntrol_btn_link = get_field('kntrol_btn_link', $id);
$kntrol_title = get_field('kntrol_title', $id);
$kntrol_subtitle = get_field('kntrol_subtitle', $id);
 
get_header(); // подключаем header.php ?>
<section>
    <div class="container">
        <div class="row">
            <div class="<?php content_class_by_sidebar(); // функция подставит класс в зависимости от того есть ли сайдбар, лежит в functions.php ?>">
                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> <?php // контэйнер с классами и id ?>
                    <h1><?php the_title(); // заголовок поста ?></h1>
                    <?php the_content(); // контент ?>
                </article>
                <?php endwhile; // конец цикла ?>
            </div>
        </div>
    </div>
</section>

<section id="b-products" class="section b-products page-we-sold">
    <div class="container">
        <!--<div class="section-title b-products_title">
                    <h2>Виды скрапа</h2>
                    <div class="section-subtitle">Различия по содержанию железа</div>
                </div>-->
        <div class="row">
            <div class="col-ms-6 col-sm-6 col-md-4">
                <div class="b-products_item">
                    <div class="b-products_item_img">
                        <a href="<?php echo get_page_link( 44 ); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/product/steel-tundish-after-eaf/steel-tundish-after-eaf1.jpg"
                                alt="">
                            <div class="b-products_item_label"><?php echo get_the_title( 44 ) ?></div>
                        </a>
                    </div>
                    <a href="<?php echo get_page_link( 44 ); ?>" class="btn btn-fill btn-w100">Подробней</a>
                </div>
            </div>
            <div class="col-ms-6 col-sm-6 col-md-4">
                <div class="b-products_item">
                    <div class="b-products_item_img">
                        <a href="<?php echo get_page_link( 39 ); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/product/steel-scrap-after-bof-slag-processing/steel-scrap-after-bof-slag-processing1.jpg"
                                alt="Steel scrap after BOF slag processing">
                            <div class="b-products_item_label"><?php echo get_the_title( 39 ) ?></div>
                        </a>
                    </div>
                    <a href="<?php echo get_page_link( 39 ); ?>" class="btn btn-fill btn-w100">Подробней</a>
                </div>
            </div>
            <div class="col-ms-6 col-sm-6 col-md-4">
                <div class="b-products_item">
                    <div class="b-products_item_img">
                        <a href="<?php echo get_page_link( 42 ); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/product/steel-scrap-after-eaf-slag-processing/steel-scrap-after-eaf-slag-processing1.jpg"
                                alt="Steel scrap after EAF slag processing">
                            <div class="b-products_item_label"><?php echo get_the_title( 42 ) ?></div>
                        </a>
                    </div>
                    <a href="<?php echo get_page_link( 42 ); ?>" class="btn btn-fill btn-w100">Подробней</a>
                </div>
            </div>
            <div class="col-ms-6 col-sm-6 col-md-4">
                <div class="b-products_item">
                    <div class="b-products_item_img">
                        <a href="<?php echo get_page_link( 37 ); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/product/pig-iron-scrap/pig-iron-scrap1.jpg" alt="">
                            <div class="b-products_item_label"><?php echo get_the_title( 37 ) ?></div>
                        </a>
                    </div>
                    <a href="<?php echo get_page_link( 37 ); ?>" class="btn btn-fill btn-w100">Подробней</a>
                </div>
            </div>
            <div class="col-ms-6 col-sm-6 col-md-4">
                <div class="b-products_item">
                    <div class="b-products_item_img">
                        <a href="<?php echo get_page_link( 16 ); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/product/cast-iron-blocks/cast-iron-blocks.jpg"
                                alt="Cast iron blocks">
                            <div class="b-products_item_label"><?php echo get_the_title( 16 ) ?></div>
                        </a>

                    </div>
                    <a href="<?php echo get_page_link( 16 ); ?>" class="btn btn-fill btn-w100">Подробней</a>
                </div>
            </div>
            <div class="col-ms-6 col-sm-6 col-md-4">
                <div class="b-products_item">
                    <div class="b-products_item_img">
                        <a href="<?php echo get_page_link( 14 ); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/product/cast-iron-scrap/cast-iron-scrap1.jpg"
                                alt="Cast iron scrap">
                            <div class="b-products_item_label"><?php echo get_the_title( 14 ) ?></div>
                        </a>
                    </div>
                    <a href="<?php echo get_page_link( 14 ); ?>" class="btn btn-fill btn-w100">Подробней</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="b-video in-page" id="b-video">
        <div class="container">
            <div class="section-title">
                <h2><?php echo $kntrol_title; ?></h2>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="row">
                        <div class="b-video_wrap">
                            <?php echo $kntrol_video; ?>
                        </div>
                    </div>
                    <p style="    font-size: 24px;
    text-align: center;
    margin-top: -10px;"><?php echo $kntrol_text; ?></p>
                    <h3 style="    text-align: center;
    margin-top: 30px;
    font-size: 31px;color: #57707B;"><?php echo $kntrol_subtitle; ?></h3>
                    <div style="    text-align: center;
    margin-top: 20px;"><a href="<?php echo $kntrol_btn_link; ?>" target="_blank"
                            class="btn btn-fill"><?php echo $kntrol_btn_text; ?></a></div>
                </div>
            </div>
        </div>
    </div>

</section>

<?php get_footer(); // подключаем footer.php ?>