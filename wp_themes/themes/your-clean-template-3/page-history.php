<?php
/**
 * Страница - История компании (page-history.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: Страница - История компании
 */
 
get_header(); // подключаем header.php ?>

	<section>
		<div class="container">
			<div class="row">
				<div class="<?php content_class_by_sidebar(); // функция подставит класс в зависимости от того есть ли сайдбар, лежит в functions.php ?>">
					<?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> <?php // контэйнер с классами и id ?>
							<h1><?php the_title(); // заголовок поста ?></h1>
							<?php the_content(); // контент ?>
						</article>
					<?php endwhile; // конец цикла ?>
				</div>
			</div>
		</div>
	</section>

        <section id="b-history" class="section b-history page-history">
            <div class="container">
			
				<div class="b-history_line_h col-sm-3">
				
					<div class="timeline-dots-h">
						<div class="timeline-dots-line-h" style="height: 1765px;"></div>
						<ul>
							<li><a href="#" class="active">0</a></li>
							<li><a href="#" class="active">1</a></li>
							<li><a href="#" class="active">2</a></li>
							<li><a href="#" class="active">3</a></li>
							<li><a href="#" class="active">4</a></li>
							<li><a href="#" class="active">5</a></li>
							<li><a href="#" class="active">6</a></li>
							<li><a href="#" class="active">7</a></li>
							<li><a href="#" class="active">8</a></li>
							<li><a href="#" class="active">9</a></li>
							<li><a href="#" class="active">10</a></li>
							<li><a href="#" class="active">11</a></li>
						</ul>
					</div>
			
				</div>
			
                <div class="b-history_inner_h col-sm-9">
				
                    <div id="timeline-h" class="timeline-h">
                        <ul class="timeline-data-h">
							<li>
                                <h3 class="timeline-title">2006</h3>
                                <p class="timeline-intro">
                                    Учреждение компании, специализирующийся на снабжении металлургических предприятий металлоломом, металлосодержащими шлаками, скрапом.
                                </p>
                            </li>
							<li>
                                <h3 class="timeline-title">2006-2008</h3>
                                <p class="timeline-intro">
                                    Комплексная рекультивация отвалов НДТЗ им. Карла Либнехта, г. Днепропетровск.
                                </p>
                            </li>
							<li>
                                <h3 class="timeline-title">2005-2008</h3>
                                <p class="timeline-intro">
                                    Комплексная рекультивация отвалов Харьковского тракторного завода.
                                </p>
                            </li>
							<li>
                                <h3 class="timeline-title">2007</h3>
                                <p class="timeline-intro">
                                    Открыто технико-технологическое подразделение для адаптации металлургических печей для использования вторичных металлургических шлаков.
                                </p>
                            </li>
							<li>
                                <h3 class="timeline-title">2009-2014</h3>
                                <p class="timeline-intro">
                                    Комплексная рекультивация отвалов Таганрогского металлургического комбината.
                                </p>
                            </li>
							<li>
                                <h3 class="timeline-title">2010</h3>
                                <p class="timeline-intro">
                                    Заключение контрактов на поставку скрапа в ПАО АМЗ Ашинский металлургический завод.
                                </p>
                            </li>
							<li>
                                <h3 class="timeline-title">2011-2014</h3>
                                <p class="timeline-intro">
                                    Рекультивация отвалов Группы Новолипецкого металлургического комбината.
                                </p>
                            </li>
							<li>
                                <h3 class="timeline-title">2013</h3>
                                <p class="timeline-intro">
                                    Открыт отдел экспорта, для поставок металлосодержащего шлака и скрапа мировым металлургическим заводам.
                                </p>
                            </li>
							<li>
                                <h3 class="timeline-title">2014</h3>
                                <p class="timeline-intro">
                                    Отгружен первый корабль с металлосодержащими шлаками, скрапом для крупнейших металлургических заводов Европы.
                                </p>
                            </li>
                            <li>
                                <h3 class="timeline-title"><img src="<?php echo get_template_directory_uri(); ?>/img/history-icon.png" alt=""> 2015</h3>
                                <p class="timeline-intro">
                                    ООО «ТК «МетСнаб» (Smartscrap) признана компанией № 2 по России
                                    как трейдер по снабжению металлургических заводов
                                    металлосодержащими шлаками, скрапом по данным Комитета
                                    Московской Торгово-Промышленной Палаты по консалтингу, а также
                                    согласно аналитическому отчету R2-COMPLEX подготовленного
                                    IstokCorparation.
                                </p>
                            </li>
                            <li>
                                <h3 class="timeline-title">2015-2016</h3>
                                <p class="timeline-intro">
                                    Переработка отходов на полигоне промышленных отходов г. Суммы.
                                </p>
                            </li>
							<li>
                                <h3 class="timeline-title">2016</h3>
                                <p class="timeline-intro">
                                    Переработка металлургических отходов завода «Кузлит» г. Камышин, Волгоградской области. <br>
                                    Переработка металлургических отходов Руставистил.
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

<?php get_footer(); // подключаем footer.php ?>