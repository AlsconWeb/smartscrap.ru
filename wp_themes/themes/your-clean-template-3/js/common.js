jQuery(document).ready(function($){
	$('.slider-banner').slick({
		dots:true,
		arrows:false,
	});
	$('.projects-slider').slick({
		dots:true,
		arrows:false,
		speed: 500,
		fade: true,
		cssEase: 'linear',
		adaptiveHeight: true
	});
	$('.slider-banner').on('afterChange', function(event, slick, currentSlide, nextSlide){
		var slideVideo = $('.slider-banner .slick-active')
			if(slideVideo.find('video').length){
				slideVideo.find('video').trigger("play");
			}else{
				console.log('else')
				$('.slider-banner .slick-slide').find('video').trigger("pause");
			}
	});
	
});
