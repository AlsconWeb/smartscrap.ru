<?php
/**
 * Страница - О нас (page-about.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: Страница - О нас
 */
 
get_header(); // подключаем header.php ?>

	<section>
		<div class="container">
			<div class="row">
				<div class="<?php content_class_by_sidebar(); // функция подставит класс в зависимости от того есть ли сайдбар, лежит в functions.php ?>">
					<?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> <?php // контэйнер с классами и id ?>
							<div class="section-title">
								<h1><?php the_title(); // заголовок поста ?></h1>
							</div>
							<?php the_content(); // контент ?>
						</article>
					<?php endwhile; // конец цикла ?>
				</div>
			</div>
		</div>
	</section>
	
	    <section class="b-cover">
            <ul class="b-cover__slider js-slider">
                <li style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/b-slider/slide1.jpg)"></li>
            </ul>
            <div class="b-cover__inner">
                <div class="b-cover__side js-slider-dots">
                    <h1 class="b-cover__title">Поставки скрапа</h1>
                    <p class="b-cover__description">
                        и других отходов металлургических производств с повышенным
                        контролем качества на территории рф, европы, азии
                    </p>
                </div>
                <!--<div class="b-cover__overlay">
                    <div class="b-cover__item">
                        <div class="b-cover__item-icon">
                            <span class="large">10</span>
                            <span class="small">лет</span>
                        </div>
                        <p class="b-cover__item-desc">занимаемся<br>продажей шлаков</p>
                    </div>
                    <div class="b-cover__item">
                        <div class="b-cover__item-icon">
                            <span class="large">1</span>
                            <span class="small">млн</span>
                        </div>
                        <p class="b-cover__item-desc">тонн скрапа<br>продали за все время</p>
                    </div>
                    <div class="b-cover__item">
                        <div class="b-cover__item-icon">
                            <span class="large">15</span>
                        </div>
                        <p class="b-cover__item-desc">корабельных партий<br>отгружено</p>
                    </div>
                    <div class="b-cover__item">
                        <div class="b-cover__item-icon">
                            Эксперты
                        </div>
                        <p class="b-cover__item-desc">в области качества<br>материала</p>
                    </div>
                </div>-->
            </div>
        </section>
		
		<div class="b-promo o-nas">
            <div class="container">
				<?php the_field("promo"); ?>
            </div>
        </div>

        <section id="b-features" class="section b-features">
            <div class="container">
                <div class="section-title b-features_title">
                    <h2>Наши преимущества и достижения</h2>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="b-features_item">
                            <div class="b-features_item_icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="38" height="44" viewBox="0 0 38 44">
                                	<path class="fill" fill="#9f9f9f" d="M31,16h0a11.99241,11.99241,0,0,1-9,11.606V38h6a1,1,0,0,1,0,2H10a1,1,0,0,1,0-2h6V27.606A11.99243,11.99243,0,0,1,7,16H7A7,7,0,0,1,0,9V7A.99973.99973,0,0,1,1,6H7V5A2.99981,2.99981,0,0,0,4,2H4A1,1,0,0,1,4,0H34a1,1,0,0,1,0,2h0a2.99982,2.99982,0,0,0-3,3h0V6h6a.99972.99972,0,0,1,1,1V9A7,7,0,0,1,31,16ZM18,38h2V27.949c-.33.028-.662.051-1,.051s-.67-.023-1-.051ZM2,8V9a4.99927,4.99927,0,0,0,5,5V8ZM7.978,2A4.96183,4.96183,0,0,1,9,5H9V16a10,10,0,1,0,20,0V5h0a4.96185,4.96185,0,0,1,1.022-3ZM36,8H31v6a4.99925,4.99925,0,0,0,5-5ZM7,42H31a1,1,0,0,1,0,2H7a1,1,0,0,1,0-2Z"/>
                                	<polygon class="fill" fill="#9f9f9f" points="19 7.572 20.191 11.236 24.044 11.236 20.927 13.501 22.117 17.165 19 14.901 15.883 17.165 17.073 13.501 13.956 11.236 17.809 11.236 19 7.572"/>
                                </svg>
                            </div>
                            <div>
                                <h3 class="b-features_item_title">Лидер по экспорту скрапа в России</h3>
                            </div>
                            <p class="b-features_item_desc">
                                Трейдер №2 в России по поставкам
                                металлургических шлаков на территории РФ
                                по версии Информационно-аналитического
                                агентства Рус Лом ruslomnews.com
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="b-features_item center">
                            <div class="b-features_item_icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="61.18246" height="47.51129" viewBox="0 0 61.18246 47.51129">
                                    <polygon class="fill" fill="#9f9f9f" points="19.659 38.618 18.772 39.483 18.396 39.849 18.485 40.368 19.396 45.682 14.624 43.173 14.158 42.928 13.693 43.173 8.921 45.682 9.833 40.368 9.922 39.849 9.546 39.483 5.685 35.721 11.02 34.944 11.54 34.869 11.773 34.397 14.158 29.564 16.544 34.397 16.777 34.869 17.297 34.944 20.217 35.37 20.386 34.384 17.441 33.955 14.158 27.304 10.876 33.955 3.535 35.023 8.848 40.199 7.593 47.511 14.158 44.057 20.724 47.511 19.47 40.199 19.659 38.618"/>
                                    <polygon class="fill" fill="#9f9f9f" points="57.781 35.023 50.441 33.955 47.158 27.304 43.876 33.955 40.716 34.415 40.885 35.401 44.02 34.944 44.54 34.869 44.773 34.397 47.158 29.564 49.544 34.397 49.777 34.869 50.297 34.944 55.632 35.721 51.772 39.483 51.396 39.849 51.485 40.368 52.396 45.682 47.624 43.173 47.158 42.928 46.693 43.173 41.921 45.682 42.833 40.368 42.922 39.849 42.546 39.483 41.393 38.359 41.68 40.036 41.848 40.199 40.593 47.511 47.158 44.057 53.724 47.511 52.47 40.199 57.781 35.023"/>
                                    <path class="fill" fill="#9f9f9f" d="M46.43613,19.61728l-10.979-1.596-4.909-9.946-4.908,9.946-10.979,1.596,7.946,7.74-1.877,10.935,9.818-5.162,9.818,5.162-1.876-10.935ZM37.70726,34.61675l-6.775-3.5602-.38409-.20339-.38409.20255-6.775,3.5602,1.296-7.54483.07312-.42863-.311-.3-5.48149-5.3403,7.57593-1.10269.42948-.06219.18994-.38745,3.38706-6.86238,3.38874,6.86321.18995.38745.42948.06219,7.57509,1.10269L36.65,26.34324l-.31013.3.07312.42863Z"/>
                                    <rect class="fill" fill="#9f9f9f" x="7.01433" y="0.70916" width="1.14998" height="14.86121" transform="translate(-3.49014 7.09138) rotate(-41.59881)"/>
                                    <rect class="fill" fill="#9f9f9f" x="4.71457" y="11.9138" width="1.1488" height="10.84979" transform="translate(-12.80908 16.30731) rotate(-69.77458)"/>
                                    <rect class="fill" fill="#9f9f9f" x="16.78802" y="-0.10236" width="1.15117" height="13.03659" transform="translate(-1.10471 5.15878) rotate(-16.38534)"/>
                                    <rect class="fill" fill="#9f9f9f" x="46.15694" y="7.56575" width="14.86121" height="1.14998" transform="translate(11.92246 42.80943) rotate(-48.40119)"/>
                                    <rect class="fill" fill="#9f9f9f" x="50.46859" y="16.76811" width="10.84979" height="1.1488" transform="translate(-2.54915 20.39254) rotate(-20.22542)"/>
                                    <rect class="fill" fill="#9f9f9f" x="37.2967" y="5.84224" width="13.03659" height="1.15117" transform="translate(25.29778 46.64288) rotate(-73.61466)"/>
                                </svg>
                            </div>
                            <div>
                                <h3 class="b-features_item_title">Имеем все лицензии</h3>
                            </div>
                            <p class="b-features_item_desc">
                                Имеем все необходимые лицензии
                                на экспорт отходов и на работу
                                с опасными отходами
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="b-features_item">
                            <div class="b-features_item_icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="62" height="54" viewBox="0 0 62 54">
                                    <path class="fill" fill="#9f9f9f" d="M15.489,40.984c.17.003.34.003.511.007V34h.969c.002-.667.026-1.333.03-2H15a.99972.99972,0,0,0-1,1v7.865A3.975,3.975,0,0,1,15.489,40.984Z"/>
                                    <path class="fill" fill="#9f9f9f" d="M61.383,21.076a.99773.99773,0,0,0-1.09.217L52,29.586V22a.99973.99973,0,0,0-1.707-.707L42,29.586V22a.99973.99973,0,0,0-1.707-.707L29.586,32H27.439a6.52766,6.52766,0,0,1,.146,2H30a.99736.99736,0,0,0,.707-.293L40,24.414V32a.99973.99973,0,0,0,1.707.707L50,24.414V32a.99973.99973,0,0,0,1.707.707L60,24.414V52H17.587a.51494.51494,0,0,1-.652.182c-.089-.042-.184-.081-.278-.121a.49822.49822,0,0,1-.282.127A21.90109,21.90109,0,0,1,14,52.259V53a.99972.99972,0,0,0,1,1H61a.99972.99972,0,0,0,1-1V22A1.00058,1.00058,0,0,0,61.383,21.076Z"/>
                                    <path class="fill" fill="#9f9f9f" d="M28.02,41A.1336.1336,0,0,0,28,41H15a.99966.99966,0,0,1-.989-1.142l3-21A.99855.99855,0,0,1,18,18h8a1.00027,1.00027,0,0,1,.996.905l1.981,20.803A1.00166,1.00166,0,0,1,28.02,41ZM16.153,39H26.9L25.09,20H18.866Z"/>
                                    <path class="fill" fill="#9f9f9f" d="M15.02,41A.1336.1336,0,0,0,15,41H3a.99981.99981,0,0,1-.998-1.071l2.769-39A1.00038,1.00038,0,0,1,5.769,0h7.385a.99962.99962,0,0,1,.999.953l1.836,38.801A.99906.99906,0,0,1,15.02,41ZM4.074,39h9.878L12.2,2H6.7Z"/>
                                    <path class="fill" fill="#9f9f9f" d="M57.5,49h-5a.50018.50018,0,0,1-.5-.5v-12a.50018.50018,0,0,1,.5-.5h5a.50018.50018,0,0,1,.5.5v12A.50018.50018,0,0,1,57.5,49ZM53,48h4V37H53Z"/>
                                    <path class="fill" fill="#9f9f9f" d="M48.5,49h-5a.50018.50018,0,0,1-.5-.5v-12a.50018.50018,0,0,1,.5-.5h5a.50018.50018,0,0,1,.5.5v12A.50018.50018,0,0,1,48.5,49ZM44,48h4V37H44Z"/>
                                    <path class="fill" fill="#9f9f9f" d="M39.5,49h-5a.50018.50018,0,0,1-.5-.5v-12a.50018.50018,0,0,1,.5-.5h5a.50018.50018,0,0,1,.5.5v12A.50018.50018,0,0,1,39.5,49ZM35,48h4V37H35Z"/>
                                    <path class="fill" fill="#9f9f9f" d="M22,15a5.99956,5.99956,0,0,1,6.037-6.077c2.076,0,3.115,1.396,3.951,2.519.813,1.094,1.323,1.712,2.346,1.712,1.071,0,1.593-.701,2.354-1.846.707-1.063,1.587-2.385,3.312-2.385a1,1,0,0,1,0,2c-.592,0-.956.455-1.646,1.493-.77,1.155-1.822,2.737-4.02,2.737-2.076,0-3.115-1.396-3.951-2.519-.813-1.094-1.323-1.712-2.346-1.712A3.97579,3.97579,0,0,0,24,15a1,1,0,0,1-2,0Z"/>
                                    <path class="fill" fill="#9f9f9f" d="M22,9.077A5.99956,5.99956,0,0,1,28.037,3c2.076,0,3.115,1.396,3.951,2.519.813,1.094,1.323,1.712,2.346,1.712,1.071,0,1.593-.701,2.354-1.846C37.396,4.322,38.275,3,40,3a1,1,0,0,1,0,2c-.592,0-.956.455-1.646,1.493-.77,1.155-1.822,2.737-4.02,2.737-2.076,0-3.115-1.396-3.951-2.519C29.569,5.618,29.06,5,28.037,5A3.975,3.975,0,0,0,24,9.077a1,1,0,0,1-2,0Z"/>
                                    <path class="fill" fill="#9f9f9f" d="M31,39H1a.99972.99972,0,0,0-1,1V53a.99972.99972,0,0,0,1,1H31a.99972.99972,0,0,0,1-1V40A.99972.99972,0,0,0,31,39Zm-1,2v7H2V41ZM2,52V49H17.138c.664-.028,1.327-.03,1.981,0H30v3Z"/>
                                </svg>
                            </div>
                            <div>
                                <h3 class="b-features_item_title">Ведущий поставщик</h3>
                            </div>
                            <p class="b-features_item_desc">
                                Ведущий поставщик Ашинского металлургического
                                завода, Новоросметалл, ОМЗ-Спецстал, ММК.<br>
                                В числе наших клиентов: Duferco, НЛМК, Северсталь, Мечел.
                            </p>
                            
                        </div>
                    </div>
					<div class="col-xs-12" style="text-align: center;">
						<img class="b-features_item_img" src="<?php echo get_template_directory_uri(); ?>/img/b-features/logotypes.png" alt="">&emsp;&emsp;<img class="b-features_item_img" src="<?php echo get_template_directory_uri(); ?>/img/b-features/logotypes.png" alt="">&emsp;&emsp;<img class="b-features_item_img" src="<?php echo get_template_directory_uri(); ?>/img/b-features/logotypes.png" alt="">
					</div>
				</div>
            </div>
        </section>
		
		<section id="b-quality2" class="section b-quality2">
            <div class="container">
                <div class="section-title b-quality2_title">
                    <h2>Все источники материалов наши эксперты оценивают при выезде и исследуют их качество надежными способами:</h2>
                </div>
                <div class="row">
					<div class="col-md-12">
                        <div class="b-quality2_item">
                            <div>
								<div class="container title-line">
									<div class="title-line-left"></div><div class="title-line-right"></div>
								</div>
                                <h3 class="b-quality2_item_title"><span>Высылаем образцы нашего материала для оценки качества</span></h3>
                            </div>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="b-quality2_item">
                            <div>
								<div class="container title-line">
									<div class="title-line-left"></div><div class="title-line-right"></div>
								</div>
                                <h3 class="b-quality2_item_title"><span>Все источники материалов наши эксперты оценивают при выезде и исследуют их качество надежными способами: проводят инспекцию, берут химию</span></h3>
                            </div>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="b-quality2_item">
                            <div>
								<div class="container title-line">
									<div class="title-line-left"></div><div class="title-line-right"></div>
								</div>
                                <h3 class="b-quality2_item_title"><span>Экспертный просчет логистики</span></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		
		
		<!--<section id="b-history" class="section b-history">
            <div class="container">
                <div class="section-title b-history_title">
                    <h2>История компании</h2>
                </div>
                <div class="b-history_inner">
                    <div id="timeline" class="timeline">
                        <ul class="timeline-data">
                            <li class="active">
                                <h3 class="timeline-title">2016</h3>
                                <p class="timeline-intro">
                                    Переработка металлургических отходов завода «Кузлит» г. Камышин, Волгоградской области. <br>
                                    Переработка металлургических отходов Руставистил.
                                </p>
                            </li>
                            <li>
                                <h3 class="timeline-title">2015-2016</h3>
                                <p class="timeline-intro">
                                    Переработка отходов на полигоне промышленных отходов г. Суммы.
                                </p>
                            </li>
                            <li>
                                <h3 class="timeline-title"><img src="<?php echo get_template_directory_uri(); ?>/img/history-icon.png" alt=""> 2015</h3>
                                <p class="timeline-intro">
                                    ООО «ТК «МетСнаб» (Smartscrap) признана компанией № 2 по России
                                    как трейдер по снабжению металлургических заводов
                                    металлосодержащими шлаками, скрапом по данным Комитета
                                    Московской Торгово-Промышленной Палаты по консалтингу, а также
                                    согласно аналитическому отчету R2-COMPLEX подготовленного
                                    IstokCorparation.
                                </p>
                            </li>
                            <li>
                                <h3 class="timeline-title">2014</h3>
                                <p class="timeline-intro">
                                    Отгружен первый корабль с металлосодержащими шлаками, скрапом для крупнейших металлургических заводов Европы.
                                </p>
                            </li>
                            <li>
                                <h3 class="timeline-title">2013</h3>
                                <p class="timeline-intro">
                                    Открыт отдел экспорта, для поставок металлосодержащего шлака и скрапа мировым металлургическим заводам.
                                </p>
                            </li>
                            <li>
                                <h3 class="timeline-title">2011-2014</h3>
                                <p class="timeline-intro">
                                    Рекультивация отвалов Группы Новолипецкого металлургического комбината.
                                </p>
                            </li>
                            <li>
                                <h3 class="timeline-title">2010</h3>
                                <p class="timeline-intro">
                                    Заключение контрактов на поставку скрапа в ПАО АМЗ Ашинский металлургический завод.
                                </p>
                            </li>
                            <li>
                                <h3 class="timeline-title">2009-2014</h3>
                                <p class="timeline-intro">
                                    Комплексная рекультивация отвалов Таганрогского металлургического комбината.
                                </p>
                            </li>
                            <li>
                                <h3 class="timeline-title">2007</h3>
                                <p class="timeline-intro">
                                    Открыто технико-технологическое подразделение для адаптации металлургических печей для использования вторичных металлургических шлаков.
                                </p>
                            </li>
                            <li>
                                <h3 class="timeline-title">2005-2008</h3>
                                <p class="timeline-intro">
                                    Комплексная рекультивация отвалов Харьковского тракторного завода.
                                </p>
                            </li>
                            <li>
                                <h3 class="timeline-title">2006-2008</h3>
                                <p class="timeline-intro">
                                    Комплексная рекультивация отвалов НДТЗ им. Карла Либнехта, г. Днепропетровск.
                                </p>
                            </li>
                            <li>
                                <h3 class="timeline-title">2006</h3>
                                <p class="timeline-intro">
                                    Учреждение компании, специализирующийся на снабжении металлургических предприятий металлоломом, металлосодержащими шлаками, скрапом.
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>-->

<?php get_footer(); // подключаем footer.php ?>