<?php
/**
 * Шаблон отдельной записи (single.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); ?>
<section>
    <div class="container">
        <div class="row">
            <div class="<?php content_class_by_sidebar(); // функция подставит класс в зависимости от того есть ли сайдбар, лежит в functions.php ?>">
                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> <?php // контэйнер с классами и id ?>
                    <h1><?php the_title(); // заголовок поста ?></h1>
                    <div class="meta">
                        <p>Опубликовано: <?php the_time(get_option('date_format')); ?></p> <?php // дата и время создания ?>
                    </div>
                    <?php the_content(); // контент ?>

                    <div class="share">
                        Поделиться в социальных сетях:
                        <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                        <script src="//yastatic.net/share2/share.js"></script>
                        <div class="ya-share2" data-services="facebook,gplus,twitter,linkedin,lj"></div>
                    </div>

                </article>
                <?php endwhile; // конец цикла ?>

            </div>
            <?php get_sidebar();  ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>