<?php
/**
 * Запись в цикле (loop.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */ 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> <?php // контэйнер с классами и id ?>

	<div class="row">
		<?php if ( has_post_thumbnail() ) { ?>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<a href="<?php the_permalink(); ?>" class="thumbnail">
					<?php the_post_thumbnail( medium ); ?>
				</a>
			</div>
		<?php } ?>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3> <?php // заголовок поста и ссылка на его полное отображение (single.php) ?>
			
			<div class="meta">
				<p><?php the_time(get_option('date_format')); ?></p> <?php // дата и время создания ?>
			</div>
			
			<?php the_content(''); // пост превью, до more ?>
			
			<div class="share">
				<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
				<script src="//yastatic.net/share2/share.js"></script>
				<div class="ya-share2" data-services="facebook,gplus,twitter,linkedin,lj"></div>
			</div>
			
			<div class="more">
				<a href="<?php the_permalink(); ?>" class="btn btn-fill">Подробней</a>
			</div>
		</div>
	</div>
</article>