<?php
/**
 * Страница - Контроль качества (page-kontrol.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: Страница - Контроль качества
 */
 
get_header(); // подключаем header.php ?>

	<section>
		<div class="container">
			<div class="row">
				<div class="<?php content_class_by_sidebar(); // функция подставит класс в зависимости от того есть ли сайдбар, лежит в functions.php ?>">
					<?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> <?php // контэйнер с классами и id ?>
							<?php the_content(); // контент ?>
						</article>
					<?php endwhile; // конец цикла ?>
				</div>
			</div>
		</div>
	</section>



		<div class="b-video" id="b-video">
            <div class="container">
                <div class="section-title">
                    <h2>Мы контролируем качество каждой партии</h2>
                </div>
                <div class="row">
                    <div class="col-md-offset-1 col-md-10">
                        <div class="row">
                            <div class="b-video_wrap">
								<iframe width="1000" height="562" src="https://www.youtube.com/embed/yWc_O_CPlrc" frameborder="0" allowfullscreen=""></iframe>     
                            </div>
                        </div>
                         <p style="    font-size: 24px;
    text-align: center;
    margin-top: -10px;">Перед тем, как предложить скрап, мы выезжаем на производство, отбираем образцы по территории всего шлакового отвала. В дальнейшем эти образцы поступают в лабораторию, в которой они обрабатываются, после чего проводится анализ. Мы предлагаем к поставке с гарантией химического анализа только те источники, в которых результаты анализов были однородными.</p>
    <h3 style="    text-align: center;
    margin-top: 30px;
    font-size: 31px;color: #57707B;">Больше полезных видео смотрите на нашем youtube канале</h3>
    <div style="    text-align: center;
    margin-top: 20px;"><a href="https://www.youtube.com/channel/UCL_lPGsYm_44clMwBUDH8ag" target="_blank"  class="btn btn-fill">Перейти на YouTube канал</a></div>
                    </div>
                </div>
            </div>
        </div>

            <section class="vidosi" style="margin-top: 50px;">
        <div class="container">
            <div class="row">
                  <div class="col-xs-12 col-sm-4">
                      <iframe width="100%" height="230" src="https://www.youtube.com/embed/NzYjHmpxhT4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                      <iframe width="100%" height="230" src="https://www.youtube.com/embed/C_gTbiWUhTg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                      <iframe width="100%" height="230" src="https://www.youtube.com/embed/gZF8FLjV1us" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>
            </div>
        </div>
</section>
		
		<section id="b-quality" class="section b-quality">
            <div class="container">
                <div class="section-title b-quality_title">
                    <h2>Контроль качества</h2>
                    <div class="section-subtitle b-quality_subtitle">На отгрузке каждой партии присутствует наш эксперт со специальным оборудованием.</div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="b-quality_item">
                            <div class="b-quality_item_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/b-quality/item1.png" alt="">
                            </div>
                            <div>
                                <h3 class="b-quality_item_title"><span>Визуальный метод</span></h3>
                                <p>
                                    Каждая партия товара отгружается под
                                    присмотром наших контролеров качества,
                                    прошедших обучение в Школе контроля
                                    качества SmartScrap
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="b-quality_item">
                            <div class="b-quality_item_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/b-quality/item3.png" alt="">
                            </div>
                            <div>
                                <h3 class="b-quality_item_title"><span>Мобильные анализаторы</span></h3>
                                <p>
                                    Используются современные рентгенофлуо-
                                    ресценные анализаторы для определения примесей
                                    химических элементов в скрапе. Перед определением
                                    химического анализа поверхность куска обязательно
                                    зачищается болгаркой и кисточкой. Если это не сделать
                                    анализатор выдаст некорректные показания.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="b-quality_item">
                            <div class="b-quality_item_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/b-quality/item2.png" alt="">
                            </div>
                            <div>
                                <h3 class="b-quality_item_title"><span>Способ водоизмещения</span></h3>
                                <p>
                                    Данный метод основан на соотношении
                                    изменения объема вытесненной воды и массы
                                    загруженной партии в емкость с водой.
                                    Вы можете скачать инструкцию <a href="#gettest" data-scroll-to>здесь</a>.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="b-quality_item">
                            <div class="b-quality_item_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/b-quality/item4.png" alt="">
                            </div>
                            <div>
                                <h3 class="b-quality_item_title"><span>Инспекторские организации</span></h3>
                                <p>
                                    Мы привлекаем инспекторские организации при
                                    продаже корабельных партий. Пример инспекторского
                                    <a href="<?php echo get_template_directory_uri(); ?>/docs/surveillance_report.pdf" download>отчета</a> можете скачать <a href="docs/surveillance_report.pdf" download>здесь</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        
		

<?php get_footer(); // подключаем footer.php ?>