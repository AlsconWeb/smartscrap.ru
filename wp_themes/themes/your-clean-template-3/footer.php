<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
?>

<section class="b-consult">
    <div class="container">
        <!-- <form class="js-form no-modal" data-title="Получить консультацию эксперта" data-msg-type="consult">-->
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row b-consult_wrap">
                            <?php echo do_shortcode("[contact-form-7 id='234' title='Заказ бесплатной консультации']"); ?>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="b-consult_desc">
                            <h2>Закажите бесплатную консультацию</h2>
                            <p>Мы знаем об отходах металлургических производств почти все.</p>
                            <p>Мы лично проверяем качество продаваемых нами отходов по всему миру и сотрудничаем ведущими металлургами на заводах,
                                применяющих наш скрап.</p>
                            <p>Мы рады будем поделиться с Вами нашим опытом.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- </form>-->
    </div>
</section>

<section id="b-contacts" class="b-contacts">
    <div class="b-contacts_top">
        <div class="container">
            <div class="section-title">
                <h2>Контакты</h2>
            </div>
            <div class="b-contacts_overlay">
                <div class="textwidget">
                    <div class="b-contacts_overlay_title">Наш главный офис</div>
                    <div class="b-contacts_overlay_address">Anexartisias & Athinon, Nora Court, офис 203 3040, Лимассол, Кипр</div>
                    <div class="b-contacts_overlay_phone">
                        <a href="tel:+3 572 200-08-57">+3 572 200-08-57</a><br>
                        <a href="tel:+7 862 225-70-03">+7 862 225-70-03</a>
                        <a href="mailto:info@smartscrap.ru">info@smartscrap.ru</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="b-contacts_main">
        <div id="g-map" class="b-contacts_map"></div>
    </div>
</section>

<footer class="footer">
    <p>&copy; 2016 «SmartScrap»</p>
</footer>
</div>

<div class="remodal" data-remodal-id="add-request">
    <button data-remodal-action="close" class="remodal-close">
        <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 27 27">
            <path stroke="#c2c2c2" d="M 0,0 L 27,27 M 27,0 L 0,27" />
        </svg>
    </button>
    <div class="modal">
        <div class="modal-title">Оставить заявку</div>
        <form class="js-form" data-title="Оставить заявку" data-msg-type="request">
            <div class="form-row">
                <input class="input input-small" type="text" name="name" placeholder="Ваше Ф.И.О">
            </div>
            <div class="form-row">
                <input class="input input-small" type="text" name="email" placeholder="Email">
            </div>
            <div class="form-row">
                <input class="input input-small" type="text" name="phone" placeholder="Телефон">
            </div>
            <div class="form-row">
                <button class="btn btn-big btn-w100" type="submit">Отправить заявку</button>
            </div>
        </form>
    </div>
</div>

<div class="remodal" data-remodal-id="calc-cost">
    <?php echo do_shortcode('[contact-form-7 id="255" title="Рассчитать стоимость товара с доставкой"]'); ?>
</div>

<div class="remodal" data-remodal-id="order-consult">
    <button data-remodal-action="close" class="remodal-close">
        <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 27 27">
            <path stroke="#c2c2c2" d="M 0,0 L 27,27 M 27,0 L 0,27" />
        </svg>
    </button>
    <div class="modal">
        <div class="modal-title">Заказать консультацию</div>
        <form class="js-form" data-title="Наш скрап подходит" data-msg-type="consult">
            <div class="form-row">
                <input class="input input-small" type="text" name="name" placeholder="Ваше Ф.И.О">
            </div>
            <div class="form-row">
                <input class="input input-small" type="text" name="email" placeholder="Email">
            </div>
            <div class="form-row">
                <input class="input input-small" type="text" name="phone" placeholder="Телефон">
            </div>
            <div class="form-row">
                <button class="btn btn-big btn-w100" type="submit">Заказать консультацию</button>
            </div>
        </form>
    </div>
</div>

<div class="remodal" data-remodal-id="and-we-can">
    <button data-remodal-action="close" class="remodal-close">
        <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 27 27">
            <path stroke="#c2c2c2" d="M 0,0 L 27,27 M 27,0 L 0,27" />
        </svg>
    </button>
    <div class="modal">
        <div class="modal-title modal-title-small">
            На данный момент страница находится в разработке.
            Для получения более подробной информации просим заполнить заявку на консультацию.
            Наш специалист свяжется с вами и даст более подробную информацию.
        </div>
        <form class="js-form no-email" data-title="А еще мы умеем" data-msg-type="consult">
            <div class="form-row">
                <input class="input input-small" type="text" name="name" placeholder="Ваше имя">
            </div>
            <div class="form-row">
                <input class="input input-small" type="text" name="phone" placeholder="Телефон">
            </div>
            <div class="form-row">
                <button class="btn btn-big btn-w100" type="submit">Заказать консультацию</button>
            </div>
        </form>
    </div>
</div>

<div class="remodal" data-remodal-id="callback">
    <button data-remodal-action="close" class="remodal-close">
        <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 27 27">
            <path stroke="#c2c2c2" d="M 0,0 L 27,27 M 27,0 L 0,27" />
        </svg>
    </button>
    <div class="modal">
        <div class="modal-title">Заказать обратный звонок</div>
        <?php echo do_shortcode('[contact-form-7 id="215" title="Заказать обратный звонок"]'); ?>

    </div>
</div>
<style type="text/css">
.ots1 {
    padding: 65px 80px 80px;
}
</style>
<div class="remodal ots1" data-remodal-id="text1">
    <button data-remodal-action="close" class="remodal-close">
        <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 27 27">
            <path stroke="#c2c2c2" d="M 0,0 L 27,27 M 27,0 L 0,27" />
        </svg>
    </button>
    <div class="modal">
        <p>Благодаря низкому содержанию таких химических элементов, как S, P, Cu и других, сталь, произведенная из наших материалов, используется для
            производства ответственных деталей, как например в поршнях дорогих автомобилей. Благодаря большей насыпной плотности, чем у черного
            металлолома, потери при плавке соизмеримы с плавкой на черном ломе при более низкой цене поставки. Оставь свою заявку, и мы поделимся
            результатами тестовой плавки нашего материала на металлургическом заводе.</p>

    </div>
</div>
<div class="remodal ots1" data-remodal-id="text2">
    <button data-remodal-action="close" class="remodal-close">
        <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 27 27">
            <path stroke="#c2c2c2" d="M 0,0 L 27,27 M 27,0 L 0,27" />
        </svg>
    </button>
    <div class="modal">
        <p>Согласно статистическим данным, собранным за несколько лет работы наших клиентов со скрапом, мы можем констатировать факт снижения расходов
            на производство стали до 3%, что в металлургическом производстве является весьма значительным показателем. Направь нам свои контакты и мы
            поделимся секретом!
            Никто на рынке не занимается оценкой и контролем…
            Часто случается так, что мы знаем о продаваемом скрапе даже больше самого производителя. Благодаря отработанным с 2006 года стандартам
            оценки качества продукции, мы понимаем, что продаем. При отгрузке продукции контроль качества производится на каждом этапе перевозки.
            Оставь заявку и получи наш стандарт оценки качества продукции.
            Наши технические специалисты готовы…
            Мы гордимся нашими металлургами, которые получили огромный опыт работы на металлургических предприятиях СНГ. Ими поставлен непризнанный
            мировой рекорд переплавки 20-тонных шлаковых чаш в 120-тонной печи без металлолома. Как пример, на одном из заводов при техническом
            сопровождении плавки скрапа параллельно мы снизили расход электроэнергии на 20% по сравнению с их обычными средними показателями.</p>

    </div>
</div>
<div class="remodal ots1" data-remodal-id="text3">
    <button data-remodal-action="close" class="remodal-close">
        <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 27 27">
            <path stroke="#c2c2c2" d="M 0,0 L 27,27 M 27,0 L 0,27" />
        </svg>
    </button>
    <div class="modal">
        <p>Часто случается так, что мы знаем о продаваемом скрапе больше производителя. Благодаря отработанным с 2006 года стандартам оценки качества
            продукции, мы понимаем, что продаем. При отгрузке продукции контроль качества происходит на каждом этапе перевозки. Получи наш стандарт
            оценки качества продукции…</p>

    </div>
</div>
<div class="remodal ots1" data-remodal-id="text4">
    <button data-remodal-action="close" class="remodal-close">
        <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 27 27">
            <path stroke="#c2c2c2" d="M 0,0 L 27,27 M 27,0 L 0,27" />
        </svg>
    </button>
    <div class="modal">
        <p>Мы гордимся нашими металлургами, которые получили огромный опыт работы на металлургических предприятиях СНГ. Ими поставлен непризнанный
            мировой рекорд переплавки 20-тонных шлаковых чаш в 120-тонной печи без металлолома. Как пример, техническом </p>

    </div>
</div>
<div class="remodal" data-remodal-id="thanks">
    <button data-remodal-action="close" class="remodal-close">
        <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 27 27">
            <path stroke="#c2c2c2" d="M 0,0 L 27,27 M 27,0 L 0,27" />
        </svg>
    </button>
    <div class="modal">
        <div class="modal-message"></div>
    </div>
</div>

<div class="remodal" data-remodal-id="download">
    <div class="modal">
        <h3 class="modal-title">Получить <span>технические характеристики оборудования</span></h3>
        <?php echo do_shortcode("[contact-form-7 id='236' title='Получить технические характеристики оборудования']"); ?>

    </div>
</div>

<div class="remodal" data-remodal-id="thanks" data-remodal-options="hashTracking: false">
    <button data-remodal-action="close" class="remodal-close"></button>
    <div class="modal">
        <h3 class="modal-title">Благодарим Вас <span>за заявку</span></h3>
        <p class="modal-message">Наш специалист свяжется с Вами в течении 24 часов</p>
    </div>
</div>

<div class="remodal" data-remodal-id="thanks_download" data-remodal-options="hashTracking: false">
    <button data-remodal-action="close" class="remodal-close"></button>
    <div class="modal">
        <h3 class="modal-title">Благодарим Вас <span>за обращение</span></h3>
        <p class="modal-message"><a href='docs/estimation_water_procedure.docx' download>Скачать тест</a></p>
    </div>
</div>

<div class="remodal" data-remodal-id="error" data-remodal-options="hashTracking: false">
    <button data-remodal-action="close" class="remodal-close"></button>
    <div class="modal">
        <h3 class="modal-title">Произошла <span>ошибка</span></h3>
        <p class="modal-message">При отправке произошла ошибка, пожалуйста попробуйте позже</p>
    </div>
</div>

<script>
window.SITE_GLOBALS = {
    messages: {
        consult: 'Запрос на консультацию отправлен. Наш менеджер свяжется с вами в течение 4-х рабочих часов. Благодарим Вас за обращение!',
        request: 'Заявка оформлена. Наш менеджер свяжется с вами в течение 4-х рабочих часов. Благодарим Вас за обращение!',
        file: 'Благодарим Вас за обращение! <a href="docs/estimation_water_procedure.docx" download>Скачать тест</a>',
        error: 'При отправке произошла ошибка, пожалуйста попробуйте позже.'
    }
};
</script>
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous">
</script>
<script>
window.jQuery || document.write(
    '<script src="http://dev.miklash.by/wp-content/themes/your-clean-template-3/js/vendor/jquery-2.2.4.min.js"><\/script>')
</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.05f26ddc.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_yKe87hwknHqry0bzVMG_qKAhfh07haQ&callback=initMap"></script>
<script>

function initMap() {
    var map, marker, location = {
        lat: 34.675429,
        lng: 33.046369
    };
    map = new google.maps.Map(document.getElementById('g-map'), {
        zoom: 14,
        scrollwheel: false
    });
    marker = new google.maps.Marker({
        position: location,
        icon: '/wp-content/themes/your-clean-template-3/img/marker-icon.png',
        map: map
    });
    if (window.innerWidth > 768) {
        location.lng = 33.046369;
    } else {
        location.lat = 34.675429;
    }
    map.setCenter(location);
}
$("p").each(function(){
	var lengthText = $(this).text();
	if(lengthText.length == 0){
		$(this).remove();
	}
})
</script>

<?php wp_footer(); ?>
</body>

</html>