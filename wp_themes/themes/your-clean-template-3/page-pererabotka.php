<?php
/**
 * Страница с кастомным шаблоном (page-pererabotka.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: Страница Переработка шлакоотвалов
 */
 
get_header(); // подключаем header.php ?>


    <div class="b-slider">
            <div class="js-slider">
                <div>
                    <div class="b-slider__slide" style="background-image:url(<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-slider/slide1.jpg)">
                        <div class="container">
                            <div class="b-slider__inner">
                                <!--<a href="#" class="b-slider__more" target="_blank">Продажа продуктов отвала</a>-->
                                <div class="b-slider__lside">
                                    <p class="b-slider__desc1">
										<?php the_field("text_slide"); ?>
                                    </p>
                                </div>
                                <div class="b-slider__rside">
                                    <p class="b-slider__desc2"><?php the_field("kolichestvo"); ?></p>
                                    <p class="b-slider__desc3">
                                        <?php the_field("text_slide2"); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="section b-sureness">
            <div class="container">
                <div class="section-title b-sureness_title">
                    <h2><span>Вы уверены,</span> что эффективно<br>перерабатываете шлакоотвал?</h2>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <p class="section-description">
							<?php the_field("block2-text"); ?>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="section b-explore">
            <div class="container">
                <div class="section-title section-title-right b-explore_title">
                    <h2><span>Узнайте больше</span> о самой эффективной<br>переработке шлаковых отвалов</h2>
                    <p>Свяжитесь с нашим экспертом прямо сейчас!</p>
                </div>
				<?php echo do_shortcode("[contact-form-7 id='235' title='Узнайте больше о переработке шлаковых отвалов']"); ?>

            </div>
        </section>

        <section class="section b-holding">
            <div class="container">
                <div class="section-title b-holding_title">
                    <h2><span>Холдинг</span> «Smartscrap»</h2>
                </div>
                <p class="section-description">
                   <?php the_field("block4-text"); ?>
                </p>
            </div>
        </section>

        <section id="b-happens" class="section b-happens">
            <div class="container">
                <div class="section-title section-title-right b-happens_title">
                    <h2><span>Частые</span> ситуации у клиента</h2>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="b-happens_item" data-accordion="#event1">
                            <div class="b-happens_item_img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="50" height="55" viewBox="0 0 58 65"><path fill="#f8982d" d="M29.27 65C4.98 43.97.68 11.29.68 11.29L29.27 0l28.32 11.29S53.34 43.97 29.27 65zm-.03-59.81L6.9 14.15s3.41 26.53 22.39 43.22C48.1 40.68 51.37 14.15 51.37 14.15L29.24 5.19zm.54 39.01c-1.78 0-3.23-1.33-3.23-2.96s1.45-2.96 3.23-2.96c1.79 0 3.24 1.33 3.24 2.96s-1.45 2.96-3.24 2.96zm-1.5-8.88l-1.73-16.56c0-1.25.97-3.16 3.02-3.16H30c2.01 0 3.02 1.79 3.02 3.16l-1.73 16.56h-3.01z"/></svg>
                            </div>
                            <p class="b-happens_item_intro"><?php the_field("block5-text1"); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="b-happens_item" data-accordion="#event2">
                            <div class="b-happens_item_img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="50" height="55" viewBox="0 0 57 65"><path fill="#f8982d" d="M28.68 65C4.39 43.97.09 11.29.09 11.29L28.68 0 57 11.29S52.74 43.97 28.68 65zm-.03-59.81L6.31 14.15S9.72 40.68 28.7 57.37c18.8-16.69 22.08-43.22 22.08-43.22L28.65 5.19zM27.25 39c-.28-.09-7.76-7.8-7.76-7.8l2.59-3.9s3.11 3.11 5.17 5.2c4.65-4.68 11.64-13 11.64-13s2.59 4.67 2.59 5.2C34.61 31.59 27.25 39 27.25 39z"/></svg>
                            </div>
                            <p class="b-happens_item_intro"><?php the_field("block5-text2"); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="b-happens_item" data-accordion="#event3">
                            <div class="b-happens_item_img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="50" height="55" viewBox="0 0 57 65"><path fill="#f8982d" d="M28.59 65C4.3 43.97 0 11.29 0 11.29L28.59 0l28.32 11.29S52.65 43.97 28.59 65zm-.03-59.81L6.22 14.15s3.41 26.53 22.39 43.22c18.8-16.69 22.08-43.22 22.08-43.22L28.56 5.19zm2.98 33.18c-2.47 1.07-6.88 1.26-9.87-3.26-4.12-6.21-2.23-12.88-2.23-12.88.84 11.09 11.54 14.2 13.56 14.69.64.12 4.63.83 6.99 0 0 0-3.19 1.89-8.45 1.45zm-1.36-3.67c-.58-1.16-2.04-4.42-1.9-7.85 0 0-1.2 2.63-.31 6.67-.91-.56-1.84-1.23-2.71-2.05-.28-.88-1.18-4.16-.47-7.36 0 0-1.36 1.96-1.36 5.35-.7-.94-1.31-2.01-1.79-3.22-.13-.89-.34-3.29.5-5.5 0 0-.84.86-1.3 2.74-.43-2.38-.38-5.18.41-8.48 0 0 2.38 4.09 8.03 8.02 7.61 5.28 4.82 13.01 4.82 13.01s-1.71-.33-3.92-1.33z"/></svg>
                            </div>
                            <p class="b-happens_item_intro"><?php the_field("block5-text3"); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="b-efficiency" class="section b-efficiency">
            <div class="container">
                <div class="section-title b-efficiency_title">
                    <h2><span>Почему</span> мы эффективны:</h2>
                    <p>
                        <span>Весь шлак перерабатывается только 1 раз и раздеееляется по продуктам!</span><br>
                        А также:
                    </p>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-lg-3">
                        <div class="b-efficiency_item">
                            <div class="b-efficiency_item_icon">
                                <div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-efficiency/items/item1.png" alt="">
                                </div>
                            </div>
                            <p class="b-efficiency_item_desc">
                                Производительность от 300000 до 2 млн тонн шлака в год
                            </p>
                            <h4 class="b-efficiency_item_title">Масштабы</h4>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="b-efficiency_item">
                            <div class="b-efficiency_item_icon">
                                <div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-efficiency/items/item2.png" alt="">
                                </div>
                            </div>
                            <p class="b-efficiency_item_desc">
                                Мы сами производим оборудование индивидуально под конкретные задачи проекта
                            </p>
                            <h4 class="b-efficiency_item_title">Производитель</h4>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="b-efficiency_item">
                            <div class="b-efficiency_item_icon">
                                <div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-efficiency/items/item3.png" alt="">
                                </div>
                            </div>
                            <p class="b-efficiency_item_desc">
                                Наше оборудование извлекает до 20% железа больше, чем оборудование конкурентов
                            </p>
                            <h4 class="b-efficiency_item_title">Максимум Fe</h4>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="b-efficiency_item">
                            <div class="b-efficiency_item_icon">
                                <div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-efficiency/items/item4.png" alt="">
                                </div>
                            </div>
                            <p class="b-efficiency_item_desc">
                                Мы производим оборудование из абсолютно унифицированных узлов, позволяющие минимизировать простои оборудования.
                                Общая продолжительность простоя оборудования  не превышает 40 часов в месяц
                            </p>
                            <h4 class="b-efficiency_item_title">Ремонтопригодность</h4>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="b-efficiency_item">
                            <div class="b-efficiency_item_icon">
                                <div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-efficiency/items/item5.png" alt="">
                                </div>
                            </div>
                            <p class="b-efficiency_item_desc">
                                Удовлетворяет самым взыскательным требованиям потребителей. Один из клиентов использует 70% продукта вместо металлолома
                            </p>
                            <h4 class="b-efficiency_item_title">Знание</h4>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="b-efficiency_item">
                            <div class="b-efficiency_item_icon">
                                <div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-efficiency/items/item6.png" alt="">
                                </div>
                            </div>
                            <p class="b-efficiency_item_desc">
                                С 2006 года переработано 8 млн.тонн шлаков на 7 различных объектах  на собственно спроектированном и произведенном оборудовании
                            </p>
                            <h4 class="b-efficiency_item_title">Опыт</h4>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="b-efficiency_item">
                            <div class="b-efficiency_item_icon">
                                <div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-efficiency/items/item7.png" alt="">
                                </div>
                            </div>
                            <p class="b-efficiency_item_desc">
                                Оборудование производится под каждый конкретный объект и цели заказчика
                            </p>
                            <h4 class="b-efficiency_item_title">Индивидуальный подход</h4>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-3">
                        <div class="b-efficiency_item">
                            <div class="b-efficiency_item_icon">
                                <div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-efficiency/items/item8.png" alt="">
                                </div>
                            </div>
                            <p class="b-efficiency_item_desc">
                                Мы работаем по всему миру
                            </p>
                            <h4 class="b-efficiency_item_title">География</h4>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section id="b-results" class="section b-results">
            <div class="container">
                <div class="section-title section-title-right b-results_title">
                    <h2><span>Наши</span> результаты</h2>
                </div>
                <div class="b-results_desc">
                    <p>Кейс «Переработка промышленных отходов завода<br>Rustavi metallurgical plant», Грузия, 2016 г.</p>
                    <a href="#registration" class="more">Подробнее</a>
                </div>
            </div>
        </section>

        <section class="section b-projects">
            <div class="container">
                <div class="section-title b-projects_title">
                    <h2><span>Другие</span> выполненные проекты</h2>
                </div>
                <div class="b-projects_items">
                    <div class="b-projects_item">
                        <div class="b-projects_item_img">
                            <img src="<?php the_field("proekt1-image"); ?>" alt="">
                        </div>
                        <div class="b-projects_item_side">
                            <h3 class="b-projects_item_title"><?php the_field("proekt1-name"); ?></h3>
                            <ul class="b-projects_item_info">
                                <li><span class="city"><?php the_field("proekt1-adress"); ?></span></li>
                                <li><span class="date"><?php the_field("proekt1-date"); ?></span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="b-projects_item">
                        <div class="b-projects_item_img">
                            <img src="<?php the_field("proekt2-image"); ?>" alt="">
                        </div>
                        <div class="b-projects_item_side">
                            <h3 class="b-projects_item_title"><?php the_field("proekt2-name"); ?></h3>
                            <ul class="b-projects_item_info">
                                <li><span class="city"><?php the_field("proekt2-adress"); ?></span></li>
                                <li><span class="date"><?php the_field("proekt2-date"); ?></span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="b-projects_item">
                        <div class="b-projects_item_img">
                            <img src="<?php the_field("proekt3-image"); ?>" alt="">
                        </div>
                        <div class="b-projects_item_side">
                            <h3 class="b-projects_item_title"><?php the_field("proekt3-name"); ?></h3>
                            <ul class="b-projects_item_info">
                                <li><span class="city"><?php the_field("proekt3-adress"); ?></span></li>
                                <li><span class="date"><?php the_field("proekt3-date"); ?></span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="b-projects_item">
                        <div class="b-projects_item_img">
                            <img src="<?php the_field("proekt4-image"); ?>" alt="">
                        </div>
                        <div class="b-projects_item_side">
                            <h3 class="b-projects_item_title"><?php the_field("proekt4-name"); ?></h3>
                            <ul class="b-projects_item_info">
                                <li><span class="city"><?php the_field("proekt4-adress"); ?></span></li>
                                <li><span class="date"><?php the_field("proekt4-date"); ?></span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="b-projects_item">
                        <div class="b-projects_item_img">
                            <img src="<?php the_field("proekt5-image"); ?>" alt="">
                        </div>
                        <div class="b-projects_item_side">
                            <h3 class="b-projects_item_title"><?php the_field("proekt5-name"); ?></h3>
                            <ul class="b-projects_item_info">
                                <li><span class="city"><?php the_field("proekt5-adress"); ?></span></li>
                                <li><span class="date"><?php the_field("proekt5-date"); ?></span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="b-projects_item">
                        <div class="b-projects_item_img">
                            <img src="<?php the_field("proekt6-image"); ?>" alt="">
                        </div>
                        <div class="b-projects_item_side">
                            <h3 class="b-projects_item_title"><?php the_field("proekt6-name"); ?></h3>
                            <ul class="b-projects_item_info">
                                <li><span class="city"><?php the_field("proekt6-adress"); ?></span></li>
                                <li><span class="date"><?php the_field("proekt6-date"); ?></span></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section id="b-stages" class="section b-stages">
            <div class="container">
                <div class="section-title b-stages_title">
                    <h2><span>Этапы</span> работ</h2>
                    <p>При производстве и продаже дробильно-сортировочной установки</p>
                </div>
                <div class="row b-stages_items">
                    <div class="col-md-6 col-lg-3 col-xs-12">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>01</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon1.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">Получение заявки</p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 col-xs-12">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>02</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon2.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">
                                Выезд специалиста для оценки структуры шлакового отвала.
                                1 неделя при наличии свободных специалистов
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 col-xs-12">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>03</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon3.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">
                                Проектирование дробильно- сортировочного комплекса и производственного процесса.<br>
                                2 недели
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 col-xs-12">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>04</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon4.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">
                                Заключение договора<br>
                                (зависит от заказчика)
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 col-xs-12">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>08</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon5.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">
                                Авторский надзор<br>
                                (рекомендуется)
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 col-xs-12">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>07</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon6.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">Сдача заказчику</p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 col-xs-12">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>06</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon7.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">
                                Настройка оборудования.<br>
                                1 месяц.
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3 col-xs-12">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>05</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon8.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">
                                Производство и установка дробильно-сортировочного комплекса.
                                4-6 месяцев
                            </p>
                        </div>
                    </div>

                </div>
                <div class="row flex-items-xs-center">
                    <a href="#download" class="btn btn-fill">Скачать технические характеристики оборудования и описание</a>
                </div>
            </div>
        </section>

     


<?php get_footer(); // подключаем footer.php ?>