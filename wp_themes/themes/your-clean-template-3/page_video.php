<?php
/**
 * Author: Lavigin A
 * Author Url: 
 * https://www.upwork.com/fl/alexl 
 * https://freelance.ru/lovik
 * 
 * 
 * Template Name: Страница Видео
 */

    $id = $post->ID;

    $videos = get_field('list_video', $id);
    $title = get_field('title', $id);
    $btn_link = get_field('btn_link', $id);
    $btn_text = get_field('btn_text', $id);

    get_header( );
?>
<section class="block-videos">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="videos">
                    <?php foreach($videos as $video):?>
                    <div class="item">
                        <?php echo $video['video']['iframe_video'];?>
                        <p><?php echo $video['video']['video_annotation'];?></p>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h3><?php echo $title;?></h3>
                <a href="<?php echo $btn_link; ?>" target="_blank" class="btn btn-fill"><?php echo $btn_text; ?></a>
            </div>
        </div>
    </div>
</section>

<?php 
    get_footer();
?>