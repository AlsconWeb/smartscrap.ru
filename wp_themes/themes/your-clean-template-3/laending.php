<?php
/**
 * Author: Lavigin A
 * Author Url: 
 * https://www.upwork.com/fl/alexl 
 * https://freelance.ru/lovik
 * 
 * 
 * Template Name: Лендинг
 */

$id = $post->ID;

$banners = get_field('banners', $id);

$b2_bg = get_field('b2_bg', $id);
$b2_video = get_field('b2_video', $id);
$b2_title = get_field('b2_title', $id);
$b2_subtext = get_field('b2_subtext', $id);
$b2_video_annotation = get_field('b2_video_annatation', $id);
$b2_btn_text = get_field('b2_btn_text', $id);
$b2_btn_link = get_field('b2_btn_link', $id);

$b3_title = get_field('b3_title', $id);
$b3_text = get_field('b3_text', $id);
$b3_title_2 = get_field('b3_title_2', $id);
$benefits = get_field('benefits', $id);
$b3_btn_text = get_field('b3_btn_text', $id);
$b3_btn_link = get_field('b3_btn_link', $id);

$b4_title = get_field('b4_title', $id);
$b4_bg = get_field('b4_bg',$id);
$b4_sliders = get_field('b4_sliders', $id);
$b4_btn_text = get_field('b4_btn_text', $id);
$b4_btn_link = get_field('b4_btn_link', $id);

$b5_title = get_field('b5_title', $id);
$b5_text = get_field('b5_text', $id);

$b6_title = get_field('b6_title', $id);
$b6_bg = get_field('b6_bg',$id);
$b6_sliders = get_field('b6_sliders', $id);
$b6_btn_text = get_field('b6_btn_text', $id);
$b6_btn_link = get_field('b6_btn_link', $id);

get_header( );
?>
<div class="banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg">
                <div class="slider-banner">
                    <?php foreach($banners as $banner):?>
                    <div class="item">
                        <?php if($banner['банер']["image"] != ''):?>
                        <img src="<?php echo $banner['банер']["image"];  ?>" alt="#">
                        <?php else:?>
                        <div class="video">
                            <video src="<?php echo $banner['банер']["video"];?>" autoplay="autoplay" muted="true"></video>
                        </div>
                        <?php endif;?>
                        <div class="slider-content">
                            <div class="block-left">
                                <div class="description">
                                    <h1><?php echo $banner['банер']["title"];?></h1>
                                    <p><?php echo $banner['банер']["text"];?></p>
                                </div>
                            </div>
                            <div class="block-right"><a class="btn btn-yellow btn-fill"
                                    href="<?php echo $banner['банер']["btn_url"];?>"><?php echo $banner['банер']["btn_text"];?></a></div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="description-block">
    <div class="container">
        <div class="row">
            <div class="col-lg">
                <div class="content-description">
                    <h3> <?php echo $b3_title;?></h3>
                    <p><?php echo $b3_text;?> </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="advantages">
    <div class="container">
        <div class="row">
            <div class="col-lg">
                <h2><?php echo $b3_title_2;?></h2>
                <div class="items">
                    <?php foreach($benefits as $benefit):?>
                    <div class="item">
                        <div class="b-applications__item">
                            <img class="b-applications__item-image" src="<?php echo $benefit['benefit']['icon'];?>" alt="">
                            <p class="b-applications__item-desc"><b><?php echo $benefit['benefit']['описание'];?></b></p>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div><a class="btn btn-fill" href="<?php echo $b3_btn_link;?>"><?php echo $b3_btn_text;?></a>
            </div>
        </div>
    </div>
</div>
<div class="projects" style="background-image:url(<?php echo $b4_bg;?>)">
    <div class="container">
        <div class="row">
            <div class="col-lg">
                <h2><?php echo $b4_title?></h2>
                <div class="projects-slider">
                    <?php foreach ($b4_sliders as $b4_slider):?>
                    <div class="item">
                        <h3><?php echo $b4_slider['слайд']['b4_subtitle'];?></h3>
                        <p><?php echo $b4_slider['слайд']['b4_text'];?></p>
                        <h4><?php echo $b4_slider['слайд']['image_title'];?></h4><img src="<?php echo $b4_slider['слайд']['image'];?>" alt="#">
                        <p><?php echo $b4_slider['слайд']['text_slide'];?></p>
                    </div>
                    <?php endforeach;?>
                </div><a class="btn btn-fill" href="<?php echo $b4_btn_link;?>"><?php echo $b4_btn_text;?></a>
            </div>
        </div>
    </div>
</div>
<div class="b-cover block-video" style="background: url(<?php echo $b2_bg;?>) no-repeat 50% 0; background-size: cover;">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg">
                <div class="block-left">
                    <h3><?php echo $b2_title;?></h3>
                    <p><?php echo $b2_subtext;?></p>
                </div>
                <div class="block-right">
                    <div class="video">
                        <?php echo $b2_video;?>
                    </div>
                    <div class="text-block">
                        <p><?php echo $b2_video_annotation;?></p>
                    </div><a class="btn btn-yellow" href="<?php echo $b2_btn_link;?>"><?php echo $b2_btn_text;?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="projects" style="background-image:url(<?php echo $b6_bg;?>)">
    <div class="container">
        <div class="row">
            <div class="col-lg">
                <h2><?php echo $b6_title?></h2>
                <div class="projects-slider">
                    <?php foreach ($b6_sliders as $b6_slider):?>
                    <div class="item">
                        <h3><?php echo $b6_slider['слайд']['b6_subtitle'];?></h3>
                        <p><?php echo $b6_slider['слайд']['b6_text'];?></p>
                        <h4><?php echo $b6_slider['слайд']['image_title'];?></h4><img src="<?php echo $b6_slider['слайд']['image'];?>" alt="#">
                        <p><?php echo $b6_slider['слайд']['text_slide'];?></p>
                    </div>
                    <?php endforeach;?>
                </div><a class="btn btn-fill" href="<?php echo $b6_btn_link;?>"><?php echo $b6_btn_text;?></a>
            </div>
        </div>
    </div>
</div>
<div class="description-block">
    <div class="container">
        <div class="row">
            <div class="col-lg">
                <div class="content-description center">
                    <h3> <?php echo $b5_title;?></h3>
                    <p><?php echo $b5_text;?></p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php get_footer();?>