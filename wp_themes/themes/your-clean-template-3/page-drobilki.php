<?php
/**
 * Страница для дробилок (page-drobilki.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 * Template Name: Страница для дробилок
 */
 
get_header(); // подключаем header.php ?>

		<div class="b-slider">
            <div class="js-slider slick-initialized slick-slider slick-dotted" role="toolbar">
                <div aria-live="polite" class="slick-list draggable">
					<div class="slick-track" style="opacity: 1; width: 100%;" role="listbox">
					
						<div class="slick-slide" data-slick-index="1" aria-hidden="true" style="width: 100%; position: relative; z-index: 998;" tabindex="-1" role="option" aria-describedby="slick-slide01">
							<div class="b-slider__slide" style="background-image: url(&quot;<?php echo get_template_directory_uri(); ?>/img/b-slider/slide22.jpg&quot;); min-height: 450px;">
								<div class="container">
									<div class="b-slider__inner">
										<div class="b-slider__lside">
											<h1><?php the_title(); // заголовок поста ?></h1>
										</div>
										<div class="b-slider__rside">
											<?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
												<?php the_content(); // контент ?>
											<?php endwhile; // конец цикла ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					
					</div>
				</div>
			</div>
		</div>
		
		<section class="section b-explore">
            <div class="container">
                <div class="section-title section-title-right b-explore_title">
                    <h2><span>Узнайте больше</span> о производство эффективных <br>дробильно-сортировочных установках</h2>
                    <p>Свяжитесь с нашим экспертом прямо сейчас!</p>
                </div>
				<?php echo do_shortcode("[contact-form-7 id='239' title='Узнайте больше о дробильно-сортировочных установках']"); ?>
                <!--<form class="js-form" novalidate="novalidate">
                    <input type="hidden" name="title" value="Узнайте больше о производство эффективных дробильно-сортировочных установках">
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="form-row">
                                <input class="input" type="text" name="name" placeholder="Ваше имя*">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="form-row">
                                <input class="input" type="text" name="email" placeholder="Ваш e-mail*">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="form-row">
                                <input class="input" type="text" name="phone" placeholder="Ваш телефон*">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="form-row">
                                <button class="btn btn-big btn-w100 btn-fill" type="submit">Заказать звонок</button>
                            </div>
                        </div>
                    </div>
                </form>-->
            </div>
        </section>
		
		<section id="b-stages" class="section b-stages">
            <div class="container">
                <div class="section-title b-stages_title">
                    <h2><span>Этапы</span> работ</h2>
                    <p>При производстве и продаже дробильно-сортировочной установки</p>
                </div>
                <div class="row b-stages_items">
                    <div class="col-md-6 col-lg-3">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>01</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon1.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">Получение заявки</p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>02</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon2.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">
                                Выезд специалиста для оценки структуры шлакового отвала.
                                1 неделя при наличии свободных специалистов
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>03</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon3.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">
                                Проектирование дробильно- сортировочного комплекса и производственного процесса.<br>
                                2 недели
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>04</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon4.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">
                                Заключение договора<br>
                                (зависит от заказчика)
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>08</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon5.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">
                                Авторский надзор<br>
                                (рекомендуется)
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>07</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon6.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">Сдача заказчику</p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>06</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon7.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">
                                Настройка оборудования.<br>
                                1 месяц.
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <div class="b-stages_item">
                            <div class="b-stages_item_icon">
                                <span>05</span>
                                <img src="<?php echo get_template_directory_uri(); ?>/slagprocessing/img/b-stages/items/icon8.png" alt="">
                            </div>
                            <p class="b-stages_item_desc">
                                Производство и установка дробильно-сортировочного комплекса.
                                4-6 месяцев
                            </p>
                        </div>
                    </div>

                </div>
                <div class="row flex-items-xs-center">
                    <a href="#download" class="btn btn-fill">Скачать технические характеристики оборудования и описание</a>
                </div>
            </div>
        </section>

<?php get_footer(); // подключаем footer.php ?>